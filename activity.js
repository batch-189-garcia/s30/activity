

db.fruits.insertMany([
        {
            "name": "Banana",
            "supplier": "Farmer Fruits Inc.",
            "enoughStock": 30,
            "price": 20,
            "fruitsOnSale": true
        },
        {
            "name": "Mango",
            "supplier": "Mango Magic Inc.",
            "enoughStock": 50,
            "price": 70,
            "fruitsOnSale": true
        },
        {
            "name": "Dragon Fruit",
            "supplier": "Farmer Fruits Inc.",
            "enoughStock": 10,
            "price": 60,
            "fruitsOnSale": true
        },
        {
            "name": "Grapes",
            "supplier": "Fruity Co.",
            "enoughStock": 30,
            "price": 100,
            "fruitsOnSale": true
        },
        {
            "name": "Apple",
            "supplier": "Apple Valley",
            "enoughStock": 0,
            "price": 20,
            "fruitsOnSale": false
        },
        {
            "name": "Papaya",
            "supplier": "Fruity Co.",
            "enoughStock": 15,
            "price": 60,
            "fruitsOnSale": true
        }
]);

// Use the count operator to count the total number of fruits on sale.

db.fruits.aggregate([
	{
		$match: { "fruitsOnSale": true }
	},
	{
      $count: "enoughStock"
    }
]);




// 3. Use the count operator to count the total number of fruits with stock more than 20.

db.fruits.aggregate(
  [
    {
      $match: {
        enoughStock: {
          $gt: 20
        }
      }
    },
    {
      $count: "enoughStock"
    }
  ]);


// 4. Use the average operator to get the average price of fruits onSale per supplier.

db.fruits.aggregate([
        {
            $match: {"fruitsOnSale": true}
        },
        {
            $group: {_id: "$supplier", avePrice: {$avg: "$price"}}
        },
                {
                    $sort: {avePrice: 1}
                }
    ]);


// 5. Use the max operator to get the highest price of a fruit per supplier

db.fruits.aggregate([
		{
			$group: {_id: "$supplier", avePrice: {$max: "$price"}}
		}

	]);


// 6. Use the min operator to get the lowest price of a fruit per supplier.

db.fruits.aggregate([
		{
			$group: {_id: "$supplier", avePrice: {$min: "$price"}}
		}

	]);